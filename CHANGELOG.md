## [2.1.2](https://gitlab.com/to-be-continuous/maven/compare/2.1.1...2.1.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([d5cac44](https://gitlab.com/to-be-continuous/maven/commit/d5cac44ae499b9ab3063cdb31935a2cd9d1b448a))

## [2.1.1](https://gitlab.com/to-be-continuous/maven/compare/2.1.0...2.1.1) (2021-09-23)


### Bug Fixes

* mvn-release missing reusing .mvn-base before_script ([2d87f8d](https://gitlab.com/to-be-continuous/maven/commit/2d87f8d0563eee8b6fcfa147938337180e32fa70))

# [2.1.0](https://gitlab.com/to-be-continuous/maven/compare/2.0.1...2.1.0) (2021-09-13)


### Features

* auto-detect Maven settings file ([70f04e3](https://gitlab.com/to-be-continuous/maven/commit/70f04e3c4e9fb50b471fd1d832ffcfec1d76f411))

## [2.0.1](https://gitlab.com/to-be-continuous/maven/compare/2.0.0...2.0.1) (2021-09-07)

### Bug Fixes

* maven-enforcer-plugin version upgrade ([30dcc01](https://gitlab.com/to-be-continuous/maven/commit/30dcc012c26fcdaf38ac3596906717f095a6c6bd))

## [2.0.0](https://gitlab.com/to-be-continuous/maven/compare/1.4.2...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([16ead86](https://gitlab.com/to-be-continuous/maven/commit/16ead8655048161dd52e6a699dd6a0f023e7d0d7))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

Signed-off-by: Cédric OLIVIER <cedric3.olivier@orange.com>

## [1.4.2](https://gitlab.com/to-be-continuous/maven/compare/1.4.1...1.4.2) (2021-06-15)

### Bug Fixes

* **sonar:** prevent shallow git clone (required by Sonar Scanner) ([4dbd90e](https://gitlab.com/to-be-continuous/maven/commit/4dbd90e47805b09317702ba643929f88322b94df))

## [1.4.1](https://gitlab.com/to-be-continuous/maven/compare/1.4.0...1.4.1) (2021-06-15)

### Bug Fixes

* autodetect MR when a milestone is here ([c4fbdf3](https://gitlab.com/to-be-continuous/maven/commit/c4fbdf37d3e07f7980ee152b37f0e8978a2d129e))

## [1.4.0](https://gitlab.com/to-be-continuous/maven/compare/1.3.0...1.4.0) (2021-06-10)

### Features

* move group ([df3a46f](https://gitlab.com/to-be-continuous/maven/commit/df3a46f19e33da869ffcea9e7b879029ad915a21))

## [1.3.0](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.2.0...1.3.0) (2021-06-07)

### Bug Fixes

* use curl instead of wget in get_latest_template_version script ([96b191f](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/96b191f8211e7957858ad58d2997f0a71391c534))

### Features

* **sonar:** autodetect Merge Request from current branch ([10c3058](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/10c3058fd379e1b989ac81b3caba84fbc347552c))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.1.2...1.2.0) (2021-05-18)

### Features

* add scoped variables support ([9089860](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/9089860cba63991fec586ddfe44304fe3e4df4c9))

## [1.1.2](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.1.1...1.1.2) (2021-05-12)

### Bug Fixes

* make semrel integration disableable ([dd29f28](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/dd29f287da033a6945b4f75aa768c380120438ae))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.1.0...1.1.1) (2021-05-12)

### Bug Fixes

* **forbid-snapshot-dependencies:** use CLI options ([24cfb7b](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/24cfb7bdc5ef66717bb75de99784049744bd092b))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.0.0...1.1.0) (2021-05-07)

### Features

* add forbid snapshot dependencies job ([295f385](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/295f38530193f62876f6167fa5fd118c4fa5119c))
* add semantic-release integration ([d99c6bb](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/d99c6bbf811d37e7e7152063f5f1fc5de230c37a))

## 1.0.0 (2021-05-06)

### Features

* initial release ([67ee980](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/67ee980ac5acf69b9bf9cf3c71d7a2d9c1385bd1))
