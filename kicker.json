{
  "name": "Maven",
  "description": "Build and test with [Maven](https://maven.apache.org/)",
  "template_path": "templates/gitlab-ci-maven.yml",
  "kind": "build",
  "variables": [
    {
      "name": "MAVEN_IMAGE",
      "description": "The Docker image used to run Maven - **set the version required by your project**",
      "default": "maven:latest"
    },
    {
      "name": "MAVEN_CFG_DIR",
      "description": "The Maven configuration directory",
      "default": ".m2",
      "advanced": true
    },
    {
      "name": "MAVEN_OPTS",
      "description": "[Global Maven options](http://maven.apache.org/configure.html#maven_opts-environment-variable)",
      "default": "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=${MAVEN_CFG_DIR}/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true",
      "advanced": true
    },
    {
      "name": "MAVEN_CLI_OPTS",
      "description": "Additional [Maven options](https://maven.apache.org/ref/3-LATEST/maven-embedder/cli.html) used on the command line",
      "default": "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true",
      "advanced": true
    },
    {
      "name": "MAVEN_BUILD_ARGS",
      "description": "Maven arguments for the build & test job",
      "default": "org.jacoco:jacoco-maven-plugin:prepare-agent verify org.jacoco:jacoco-maven-plugin:report",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "sonar",
      "name": "SONAR",
      "description": "Code quality and security analysis with [SONARQube](https://www.sonarqube.org/)",
      "variables": [
        {
          "name": "SONAR_URL",
          "type": "url",
          "description": "SonarQube server url",
          "mandatory": true
        },
        {
          "name": "SONAR_AUTH_TOKEN",
          "description": "SonarQube authentication [token](https://docs.sonarqube.org/latest/user-guide/user-token/) (depends on your authentication method)",
          "secret": true
        },
        {
          "name": "SONAR_LOGIN",
          "description": "SonarQube login (depends on your authentication method)",
          "secret": true
        },
        {
          "name": "SONAR_PASSWORD",
          "description": "SonarQube password (depends on your authentication method)",
          "secret": true
        },
        {
          "name": "SONAR_BASE_ARGS",
          "description": "SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/)",
          "default": "sonar:sonar -Dsonar.host.url=${SONAR_URL} -Dsonar.links.homepage=${CI_PROJECT_URL} -Dsonar.links.ci=${CI_PROJECT_URL}/-/pipelines -Dsonar.links.issue=${CI_PROJECT_URL}/-/issues",
          "advanced": true
        },
		{
          "name": "SONAR_GITLAB_TOKEN",
          "description": "GitLab API access token. When set, activates the [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties) integration, and enables SonarQube [Pull Request Analysis](https://docs.sonarqube.org/latest/analysis/pull-request/)",
          "secret": true
        },
        {
          "name": "SONAR_BRANCH_ANALYSIS_DISABLED",
          "description": "Set to disable automatic [Pull Request Analysis](https://docs.sonarqube.org/latest/analysis/pull-request/) and [Branch Analysis](https://docs.sonarqube.org/latest/branches/overview/)",
          "type": "boolean"
        },
        {
          "name": "SONAR_GITLAB_ARGS",
          "description": "Extra arguments to use with [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties)",
          "default": "-Dsonar.gitlab.url=${CI_SERVER_URL} -Dsonar.gitlab.user_token=${SONAR_GITLAB_TOKEN} -Dsonar.gitlab.project_id=${CI_PROJECT_ID} -Dsonar.gitlab.commit_sha=${CI_COMMIT_SHA} -Dsonar.gitlab.ref_name=${CI_COMMIT_REF_NAME}",
          "advanced": true
        },
        {
          "name": "SONAR_AUTO_ON_DEV_DISABLED",
          "description": "When set, SonarQube analysis becomes **manual** on development branches (automatic otherwise)",
          "type": "boolean"
        },
        {
          "name": "SONAR_QUALITY_GATE_ENABLED",
          "description": "Enable blocking check of SonarQube [Quality Gate](https://docs.sonarqube.org/latest/user-guide/quality-gates/) (for `master` branch)",
          "type": "boolean"
        }
      ]
    },
    {
      "id": "dependency-check",
      "name": "Dependency-Check",
      "description": "[Dependency-Check](https://jeremylong.github.io/DependencyCheck/dependency-check-maven/configuration.html) analysis",
      "variables": [
        {
          "name": "MAVEN_DEPENDENCY_CHECK_ARGS",
          "description": "Maven arguments for Dependency Check job",
          "default": "org.owasp:dependency-check-maven:check -DretireJsAnalyzerEnabled=false -DassemblyAnalyzerEnabled=false -Dformats=HTML,JSON,XML",
          "advanced": true
        }
      ]
    },
    {
      "id": "forbid-snapshot-dependencies",
      "name": "Snapshot dependencies verification",
      "description": "This job verifies your project has no _snapshot_ dependencies. Failure is allowed in feature branches.",
      "variables": [
        {
          "name": "MVN_FORBID_SNAPSHOT_DEPENDENCIES_DISABLED",
          "description": "Disable the job",
          "type": "boolean"
        }
      ]
    },
    {
      "id": "publish",
      "name": "Publish",
      "description": "Publish [Snapshot](https://maven.apache.org/plugins/maven-deploy-plugin/) & [Releases](http://maven.apache.org/maven-release/maven-release-plugin) to a Nexus repository",
      "enable_with": "MAVEN_DEPLOY_ENABLED",
      "variables": [
        {
          "name": "MAVEN_DEPLOY_ARGS",
          "description": "Maven arguments for the Snapshot job",
          "default": "deploy -DskipTests",
          "advanced": true
        },
        {
          "name": "MAVEN_DEPLOY_FROM_UNPROTECTED_DISABLED",
          "description": "Set to limit snapshot publication to protected branches",
          "type": "boolean",
          "advanced": true
        },
        {
          "name": "MAVEN_RELEASE_ARGS",
          "description": "Maven arguments for the Release job",
          "default": "release:prepare release:perform -DskipTests",
          "advanced": true
        },
        {
          "name": "MAVEN_RELEASE_SCM_COMMENT_PREFIX",
          "description": "Maven release plugin [scmCommentPrefix](https://maven.apache.org/maven-release/maven-release-plugin/prepare-mojo.html#scmCommentPrefix) parameter",
          "default": "[ci skip][maven-release-plugin]",
          "advanced": true
        },
        {
          "name": "MVN_SEMREL_RELEASE_DISABLED",
          "description": "Disable semantic-release integration",
          "advanced": true
        },
        {
          "name": "GIT_USERNAME",
          "description": "Git username (if you wish to release using Git credentials)",
          "secret": true
        },
        {
          "name": "GIT_PASSWORD",
          "description": "Git password (if you wish to release using Git credentials)",
          "secret": true
        },
        {
          "name": "GIT_PRIVATE_KEY",
          "description": "Git private SSH key (if you wish to release using SSH key or GitLab Deploy Key)",
          "secret": true
        },
        {
          "name": "MAVEN_REPOSITORY_USERNAME",
          "description": "Maven repository username (inject in your settings.xml as ${env.MAVEN_REPOSITORY_USERNAME})",
          "secret": true
        },
        {
          "name": "MAVEN_REPOSITORY_PASSWORD",
          "description": "Maven repository password (inject in your settings.xml as ${env.MAVEN_REPOSITORY_PASSWORD})",
          "secret": true
        }
      ]
    }
  ]
}
